# ngx-dictionary-pipe

Angular 8 pipe for easy dictionary/master data ID to name mapping.

This module is unit-tested. If there is a failing case, please submit an issue so someone can fill a test and fix it.

Documentation can be found with examples can be found <a href="http://isc.jfmr.gitlab.io/ngx-dictionary-pipe">here</a>.

Badges and other stuff are pending, check TODO list further below.

# Installation

Add `NgxDictionaryPipeModule` to your module imports like this:
```ts
// import it
import { NgxDictionaryPipeModule } from 'ngx-dictionary-pipe';

@NgModule({
  declarations: [ ... ],
  imports: [
    ...,
    NgxDictionaryPipeModule // add to module
  ],
})
export class AppModule { }

```

# How to use

Define a variable "masterData" with the following data:
```json
{
  "books": [
    {
      "id": 1,
      "name": "The dawn of the night"
    },
    {
      "id": 2,
      "name": "The dawn of the morning"
    }
  ]
}
```

Define a variable "myBooksIds" with the following data:
```json
[ 1, 3 ]
```

Then, use the `dictionary` pipe to replace ID occurrences, like this:

```html
<ul>
   <li *ngFor="let bookId in myBooksIds">
      {{ bookId | dictionary : masterData : ['books[]', 'id'] : 'name' }}
   </li>
</ul>
```

Alternatively, `['books[]', 'id']` can be written as `'books[].id'`.

This can be understood as:
```js
const valueToSearch = bookId;
const dictionary = masterData;
const path = ['books[]'];
const valueKey = 'id'; // must equal valueToSearch
const nameKey = 'name'; // resulting readable name
```

This will render the following output:
```html
<ul>
   <li>
      The dawn of the night
   </li>
   <li>
      ???
   </li>
</ul>
```

## Examples

Further documentation with examples can be found <a href="http://isc.jfmr.gitlab.io/ngx-dictionary-pipe">here</a>.

## Troubleshooting
#### Pipe is printing a blank string
This value is returned if there was an error on the dictionary search. Try setting debug = true when invoking the pipe for displaying debug errors `?`, `??` and `???`.

> e.g. instead of `{{ bookId | dictionary : masterData : ['books[]', 'id'] : 'name' }}`,
> 
> use `{{ bookId | dictionary : masterData : ['books[]', 'id'] : 'name' : true }}`,

If debug is enabled, and it is still printing a blank string, this means the value to search was undefined, null or empty. It's probably the most preferred behaviour.
#### Pipe is printing `?`
This means the path was not found, e.g. `masterData.bookse` in the previous example.
#### Pipe is printing `??`
This means the `id` key was not found, e.g. `masterData.books.ide` in the previous example.
#### Pipe is printing `???`
This means an item with the specified value was not found, e.g. `3` in the previous example.

## Is this useful?

Personally speaking, it greatly simplifies many problems I regularly face when I need to replace IDs with names.

If I had to say, it is.

## TODO:
- Document [] operator for a loop search, e.g. `stores[].books.uuid`.
- Document {} operator for a map search, e.g. `stores.books{}`.
- Document support for value inside [] and {} operators to specify a key to lookup, e.g. `stores[1].books{b}.authors` and `stores[id:1].books{name:b}.authors`.
- To consider: Accepting a function instead of `nameKey = name` for custom browsing, e.g. `myFunction(value: any, ...pathItems: any[])` where `pathItems` is the transversal stack.
- Add "bdictionary" for a binary search based dictionary (performance).
- Document debug flag.
- Simplify repository for NPM publish.
- Allow for simple and quick inheritance for enums handling, e.g. `{{ statusId | status }}`.
- Document support for [] operator at the end of query to collect multiple results instead of returning immediately (filtering).
- In consideration: Add collecting to map support, e.g. `books.id{}` would make a map `{ [id]: item }`, while `books.id{prop}` would group (?).