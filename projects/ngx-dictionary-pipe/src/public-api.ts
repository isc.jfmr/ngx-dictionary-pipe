/*
 * Public API Surface of ngx-dictionary-pipe
 */

export * from './lib/dictionary.pipe';
export * from './lib/ngx-dictionary-pipe.module';
