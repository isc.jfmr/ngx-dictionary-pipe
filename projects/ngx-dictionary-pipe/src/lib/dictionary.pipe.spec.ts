import { DictionaryPipe } from './dictionary.pipe';

describe('DictionaryPipe', () => {
  interface TestCase {
    searchValue: any;
    searchKey: any;
    path: any | any[];
    resultKey: string;
    expectedResult: any;
    debug?: boolean;
  }
  const BAD_PATH_ERROR = '?';
  const BAD_KEY_ERROR = '??';
  const VALUE_NOT_FOUND_ERROR = '???';
  // define master data for tests
  const masterData = {
    books: [
      {
        id: 1,
        name: 'The dawn of the night',
      },
      {
        id: 2,
        name: 'The dawn of the morning',
      },
    ],
    stores: [
      {
        id: 41,
        name: 'Northwood Library',
        books: [
          {
            id: 4,
            name: 'Rendezvous',
          },
          {
            id: 5,
            name: 'Conflict',
          },
          {
            id: 6,
            name: 'The dawn of the morning',
          },
        ],
      },
      {
        id: 42,
        name: 'Greenman Library',
        books: [
          {
            id: 7,
            name: 'Our tribe',
          },
          {
            id: 8,
            name: 'The one who sees it all',
          },
        ],
      },
    ],
    booksById: {
      78: {
        name: 'Claus & Clowns',
      },
      79: {
        name: 'Winter: Elves vs Comedians',
      },
    },
    storesById: {
      AA: {
        id: '100',
        name: 'PowerMax',
        books: [
          {
            id: 101,
            name: 'Batteries will dominate the world',
          },
          {
            id: 102,
            name: 'Is your battery spying on you?',
          },
        ],
      },
      BB: {
        id: '900',
        name: 'Patriots & More',
        books: [
          {
            id: 911,
            name: 'My children found my airsoft gun',
          },
          {
            id: 912,
            name: 'Counterattack',
          },
        ],
      },
    },
    section: {
      shelf: {
        books: [
          {
            id: 99,
            name: 'Get Smarter',
          },
        ],
      },
    },
  };
  const pipe: DictionaryPipe = new DictionaryPipe();
  // beforeEach(() => { pipe = new DictionaryPipe(); });

  it('#pipe should have be a constructed DictionaryPipe', () => {
    expect(pipe).not.toBeNull('Failed to construct DictionaryPipe');
  });

  it(`#transform should be able to return matching results for {{ bookId | dictionary : masterData : ['books', 'id'] : 'name' }}`, () => {
    const bookTestCases = [
      // {{ 1 | dictionary : masterData : ['books', 'id'] : 'name' }}
      {
        searchValue: 1,
        searchKey: 'id',
        path: ['books'],
        resultKey: 'name',
        expectedResult: masterData.books.find(x => x.id === 1).name,
      },
      // {{ 2 | dictionary : masterData : ['bookse', 'id'] : 'name' }}
      {
        searchValue: 2,
        searchKey: 'id',
        path: ['bookse'],
        resultKey: 'name',
        expectedResult: BAD_PATH_ERROR,
        debug: true,
      },
      // {{ 2 | dictionary : masterData : ['books', 'ide'] : 'name' }}
      {
        searchValue: 2,
        searchKey: 'ide',
        path: ['books'],
        resultKey: 'name',
        expectedResult: BAD_KEY_ERROR,
        debug: true,
      },
      // {{ 3 | dictionary : masterData : ['books', 'id'] : 'name' }}
      {
        searchValue: 3,
        searchKey: 'id',
        path: ['books'],
        resultKey: 'name',
        expectedResult: VALUE_NOT_FOUND_ERROR,
        debug: true,
      },
      // {{ 2 | dictionary : masterData : ['books', 'id'] }}
      {
        searchValue: 2,
        searchKey: 'id',
        path: ['books'],
        expectedResult: masterData.books.find(x => x.id === 2),
      },
      // {{ 1 | dictionary : masterData : 'books[].id', 'name' }}
      {
        searchValue: 1,
        searchKey: 'id',
        path: ['books[]'],
        resultKey: 'name',
        expectedResult: masterData.books.find(x => x.id === 1).name,
      },
      // {{ 4 | dictionary : masterData : 'stores[id:41].books[].id', 'name' }}
      {
        searchValue: 4,
        searchKey: 'id',
        path: ['stores[id:41]', 'books[]'],
        resultKey: 'name',
        expectedResult: masterData.stores.find(x => x.id === 41).books.find(x => x.id === 4).name,
      },
      // {{ 7 | dictionary : masterData : 'stores[id:42].books[].id', 'name' }}
      {
        searchValue: 7,
        searchKey: 'id',
        path: ['stores[id:42]', 'books[]'],
        resultKey: 'name',
        expectedResult: masterData.stores.find(x => x.id === 42).books.find(x => x.id === 7).name,
      },
      // {{ 4 | dictionary : masterData : 'stores[0].books[].id', 'name' }}
      {
        searchValue: 4,
        searchKey: 'id',
        path: ['stores[0]', 'books[]'],
        resultKey: 'name',
        expectedResult: masterData.stores[0].books.find(x => x.id === 4).name,
      },
      // {{ 99 | dictionary : masterData : 'section.shelf.books[].id', 'name' }}
      {
        searchValue: 99,
        searchKey: 'id',
        path: ['section', 'shelf', 'books[]'],
        resultKey: 'name',
        expectedResult: masterData.section.shelf.books.find(x => x.id === 99).name,
      },
      // {{ 78 | dictionary : masterData : 'booksById{}', 'name' }}
      {
        searchValue: 78,
        path: ['booksById{}'],
        resultKey: 'name',
        expectedResult: masterData.booksById['78'].name,
      },
      // {{ 101 | dictionary : masterData : 'storesById{AA}.books[].id', 'name' }}
      {
        searchValue: 101,
        searchKey: 'id',
        path: ['storesById{AA}', 'books[]'],
        resultKey: 'name',
        expectedResult: masterData.storesById.AA.books.find(x => x.id === 101).name,
      },
      // {{ 102 | dictionary : masterData : 'storesById{id:100}.books[].id', 'name' }}
      {
        searchValue: 102,
        searchKey: 'id',
        path: ['storesById{id:100}', 'books[]'],
        resultKey: 'name',
        expectedResult: masterData.storesById.AA.books.find(x => x.id === 102).name,
      },
      // {{ 911 | dictionary : masterData : 'storesById{id:900}.books[].id', 'name' }}
      {
        searchValue: 911,
        searchKey: 'id',
        path: ['storesById{id:900}', 'books[]'],
        resultKey: 'name',
        expectedResult: masterData.storesById.BB.books.find(x => x.id === 911).name,
      },
      // {{ 912 | dictionary : masterData : 'storesById{id:900}.books[].id', 'name' }}
      {
        searchValue: 912,
        searchKey: 'id',
        path: ['storesById{id:900}', 'books[]'],
        resultKey: 'name',
        expectedResult: masterData.storesById.BB.books.find(x => x.id === 912).name,
      },
    ] as TestCase[];
    for (const testCase of bookTestCases) {
      console.log('RUNNING TEST CASE:');
      console.log(printAsHTMLPipe(testCase));
      // bookId is manually named here to hint on failed case location
      const bookId = testCase.searchValue;
      const { expectedResult } = testCase;
      const result = pipe.transform(bookId, masterData, testCase.path.concat(testCase.searchKey), testCase.resultKey, testCase.debug);
      expect(result).toBe(expectedResult, `Failed test case for 'bookId': ${printAsHTMLPipe(testCase)}`);
      const alternativeNotationResult = pipe.transform(
        bookId,
        masterData,
        testCase.path.concat(testCase.searchKey).join('.'),
        testCase.resultKey,
        testCase.debug
      );
      expect(alternativeNotationResult).toBe(result, 'There was a problem with the alternative notation');
      if (testCase.debug) {
        switch (result) {
          case BAD_KEY_ERROR:
          case BAD_PATH_ERROR:
          case VALUE_NOT_FOUND_ERROR:
            expect(pipe.transform(bookId, masterData, testCase.path.concat(testCase.searchKey), testCase.resultKey, false)).toBe(
              '',
              'Unexpected result after setting debug off'
            );
            break;
        }
      }
    }
  });

  it(`#transform should be able to return matching results for {{ 11 | dictionary : enumArray : '[].id' : 'name' }}`, () => {
    const dictionary = [11, 12];
    {
      const result = pipe.transform(0, dictionary, ['[]']);
      expect(result).toBe(11, 'Failed to perform a dictionary search over an array');
    }
    {
      const result = pipe.transform(1, dictionary, ['[]']);
      expect(result).toBe(12, 'Failed to perform a dictionary search over an array');
    }
  });

  it(`#transform should be able to return matching results for {{ 11 | dictionary : enumArray : '[].id' : 'name' }}`, () => {
    const dictionary = [
      {
        id: 11,
        name: 'enumValue1',
      },
      {
        id: 12,
        name: 'enumValue2',
      },
    ];
    {
      const result = pipe.transform(11, dictionary, ['[]', 'id'], 'name');
      expect(result).toBe('enumValue1', 'Failed to perform a dictionary search over an array');
    }
    {
      const result = pipe.transform(12, dictionary, ['[]', 'id'], 'name');
      expect(result).toBe('enumValue2', 'Failed to perform a dictionary search over an array');
    }
  });

  it(`#transform should be able to return matching results for {{ 11 | dictionary : enumMap : '{}' : 'name' }}`, () => {
    const dictionary = {
      11: {
        name: 'enumValue1',
      },
      12: {
        name: 'enumValue2',
      },
    };
    {
      const result = pipe.transform(11, dictionary, '{}', 'name');
      expect(result).toBe('enumValue1', 'Failed to perform a dictionary search over a map');
    }
    {
      const result = pipe.transform(12, dictionary, '{}', 'name');
      expect(result).toBe('enumValue2', 'Failed to perform a dictionary search over a map');
    }
  });

  it(`#transform should be able to return matching results for {{ 11 | dictionary : enumMap : '{}.id' : 'name' }}`, () => {
    const dictionary = {
      a: {
        id: 11,
        name: 'enumValue1',
      },
      b: {
        id: 12,
        name: 'enumValue2',
      },
    };
    {
      const result = pipe.transform(11, dictionary, '{}.id', 'name');
      expect(result).toBe('enumValue1', 'Failed to perform a dictionary search over a map');
    }
    {
      const result = pipe.transform(12, dictionary, '{}.id', 'name');
      expect(result).toBe('enumValue2', 'Failed to perform a dictionary search over a map');
    }
  });

  it(`#transform should be able to collect and return matching results for {{ 'fruits' | dictionary : enumArray : '[].group[]' }}`, () => {
    const dictionary = [
      {
        id: 11,
        name: 'Bad Apple',
        group: 'fruits',
      },
      {
        id: 12,
        name: 'Good Apple',
        group: 'fruits',
      },
      {
        id: 13,
        name: 'lettuce',
        group: 'vegetables',
      },
    ];
    {
      const result = pipe.transform('fruits', dictionary, ['[]', 'group[]']);
      expect(result).toEqual(dictionary.filter(x => x.group === 'fruits'), 'Failed to perform a dictionary search over an array');
    }
  });

  // TODO: Consider the following cases
  // TODO: Add test for [].group{} collects into arrays with same group (always single)
  // TODO: Add test for [].group{id} collects into arrays with same id
  // TODO: Add test for [].group[id]

  function printAsHTMLPipe(testCase: TestCase, alternativeNotation?: true) {
    if (alternativeNotation) {
    } else {
      return (
        `{{ ${testCase.searchValue} | dictionary : masterData : [${testCase.path
          .concat(testCase.searchKey)
          .map(x => `'${x}'`)
          .join(`, `)}]` + (testCase.resultKey ? ` : '${testCase.resultKey}' }}` : '')
      );
    }
  }
});
