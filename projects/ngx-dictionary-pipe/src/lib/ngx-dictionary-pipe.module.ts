import { NgModule } from '@angular/core';
import { DictionaryPipe } from './dictionary.pipe';

@NgModule({
  declarations: [DictionaryPipe],
  imports: [
  ],
  exports: [DictionaryPipe]
})

export class NgxDictionaryPipeModule { }
