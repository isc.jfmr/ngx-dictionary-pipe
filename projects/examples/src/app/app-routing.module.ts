import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IntroductionComponent } from './pages/introduction/introduction.component';
import { ExamplesComponent } from './pages/examples/examples.component';
import { ScratchboardComponent } from './pages/scratchboard/scratchboard.component';
import { HowToComponent } from './pages/how-to/how-to.component';


const routes: Routes = [
  { path: '', redirectTo: '/introduction', pathMatch: 'full' },
  { path: 'introduction', component: IntroductionComponent },
  { path: 'how-to', component: HowToComponent },
  { path: 'examples', component: ExamplesComponent },
  { path: 'scratchboard', component: ScratchboardComponent },
  { path: 'scratchboard/:exampleId', component: ScratchboardComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
