import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { NGX_MONACO_EDITOR_CONFIG } from 'ngx-monaco-editor';
import { DictionaryPipe } from '../../../../dist/ngx-dictionary-pipe';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [
    {
      provide: NGX_MONACO_EDITOR_CONFIG,
      useValue: { }
    }],
  encapsulation: ViewEncapsulation.None
})
export class AppComponent {
}
