import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { IntroductionComponent } from './pages/introduction/introduction.component';
import { ScratchboardComponent } from './pages/scratchboard/scratchboard.component';
import { MenuComponent } from './pages/menu/menu.component';
import { ExamplesComponent } from './pages/examples/examples.component';

import { FlexLayoutModule } from '@angular/flex-layout';
import { NgxDictionaryPipeModule, DictionaryPipe } from '../../../../dist/ngx-dictionary-pipe';
import { NgxJsonViewerModule } from 'ngx-json-viewer';
import { MonacoEditorModule } from 'ngx-monaco-editor';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HowToComponent } from './pages/how-to/how-to.component';

@NgModule({
  declarations: [
    AppComponent,
    IntroductionComponent,
    ScratchboardComponent,
    MenuComponent,
    ExamplesComponent,
    HowToComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FlexLayoutModule,
    NgxDictionaryPipeModule,
    NgxJsonViewerModule,
    MonacoEditorModule,
    FormsModule,
    BrowserAnimationsModule,
  ],
  providers: [DictionaryPipe],
  bootstrap: [AppComponent]
})
export class AppModule { }
