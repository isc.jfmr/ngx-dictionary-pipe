import { Component, OnInit } from '@angular/core';
import { DictionaryPipe } from '../../../../../../dist/ngx-dictionary-pipe/';
import { ActivatedRoute } from '@angular/router';
import { ExamplesService } from '../examples/examples.service';

@Component({
  selector: 'app-scratchboard',
  templateUrl: './scratchboard.component.html',
  styleUrls: ['./scratchboard.component.css']
})
export class ScratchboardComponent implements OnInit {
  constructor(private dictionaryPipe: DictionaryPipe, private examplesService: ExamplesService, private route: ActivatedRoute) {
  }

  searchValue = '1';
  path = 'books[].id';
  outputKey = 'name';
  result: any;
  errorParsingJson: boolean;
  error: string;
  inputDictionary: any;
  _rawJson: string;
  debug: boolean;
  get rawJson() {
    return this._rawJson;
  }
  set rawJson(value) {
    this._rawJson = value;
    this.updateResult();
  }
  get resultType() {
    return this.result ? (Array.isArray(this.result) ? 'array' : typeof this.result) : '';
  }

  editorOptions = { theme: 'vs-light', language: 'json' };

  ngOnInit() {
    this.rawJson = JSON.stringify(
      {
        books: [
          {
            id: 1,
            name: 'The dawn of the night',
          },
          {
            id: 2,
            name: 'The dawn of the morning',
          },
        ],
        stores: [
          {
            id: 41,
            name: 'Northwood Library',
            books: [
              {
                id: 4,
                name: 'Rendezvous',
              },
              {
                id: 5,
                name: 'Conflict',
              },
              {
                id: 6,
                name: 'The dawn of the morning',
              },
            ],
          },
          {
            id: 42,
            name: 'Greenman Library',
            books: [
              {
                id: 7,
                name: 'Our tribe',
              },
              {
                id: 8,
                name: 'The one who sees it all',
              },
            ],
          },
        ],
        booksById: {
          78: {
            name: 'Claus & Clowns',
          },
          79: {
            name: 'Winter: Elves vs Comedians',
          },
        },
        storesById: {
          AA: {
            id: '100',
            name: 'PowerMax',
            books: [
              {
                id: 101,
                name: 'Batteries will dominate the world',
              },
              {
                id: 102,
                name: 'Is your battery spying on you?',
              },
            ],
          },
          BB: {
            id: '900',
            name: 'Patriots & More',
            books: [
              {
                id: 911,
                name: 'My children found my airsoft gun',
              },
              {
                id: 912,
                name: 'Counterattack',
              },
            ],
          },
        },
        section: {
          shelf: {
            books: [
              {
                id: 99,
                name: 'Get Smarter',
              },
            ],
          },
        },
      }, null, 2
    );

    this.route.params.subscribe(params => {
      if (params['exampleId'] !== undefined) {
        const example = this.examplesService.getExample(+params['exampleId']);
        this.searchValue = example.searchValue;
        this.path = example.path;
        this.outputKey = example.outputKey && example.outputKey.startsWith('\'') ? example.outputKey.substr(1, example.outputKey.length - 2) : '';
        this.rawJson = JSON.stringify(example.inputDictionary, null, 2);
      }
    });
  }

  updateResult() {
    try {
      this.result = 'Error parsing JSON';
      this.inputDictionary = JSON.parse(this.rawJson);
      this.errorParsingJson = false;
      this.error = '';
      this.result = this.dictionaryPipe.transform(this.searchValue, this.inputDictionary, this.path, this.outputKey, this.debug);
    }
    catch (err) {
      this.errorParsingJson = true;
      this.error = err;
    }
  }
}
