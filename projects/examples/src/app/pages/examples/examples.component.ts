import { Component, OnInit } from '@angular/core';
import { ExamplesService, ExampleType, ExampleData } from './examples.service';

@Component({
  selector: 'app-examples',
  templateUrl: './examples.component.html',
  styleUrls: ['./examples.component.css']
})
export class ExamplesComponent implements OnInit {
  exampleTypes: ExampleType[];
  examples: ExampleData[];
  
  constructor(private examplesService: ExamplesService) {
    this.exampleTypes = this.examplesService.getExampleTypes();
    this.examples = this.examplesService.getExamples();
  }

  ngOnInit() {
  }

}
